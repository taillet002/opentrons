
#include <Adafruit_NeoPixel.h>
// Which pin on the Arduino is connected to the NeoPixels?
#define LED_PIN 5

// How many NeoPixels are attached to the Arduino?
#define NUM_OF_LED 57

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_OF_LED, LED_PIN, NEO_GRB + NEO_KHZ800);

int delayval = 10; // delay for half a second

void setup() 
{
  strip.begin(); // This initializes the NeoPixel library.
  strip.show();
  Serial.begin(9600);
}
void switchLight(int status)
{
	int color;
	if (status) {
		color = 255;
	}
	else { color = 0;}
  for(int i=0;i<NUM_OF_LED;i++)
  {
    // strip.Color takes RGB values, from 0,0,0 up to 255,255,255
    strip.setPixelColor(i, strip.Color(color,color,color)); // white color.
     // This sends the updated pixel color to the hardware.
    //delay(delayval); // Delay for a period of time (in milliseconds).
  }
strip.show();
}
void loop() 
{
  int command;
  while(Serial.available()) {
  
    command = Serial.read();// read the incoming data as string
    if (command == 49) { switchLight(1); Serial.print(1,BIN); }
    else if (command == 48) { switchLight(0); Serial.print(0,BIN); }
    
  }
  delay(20);
  // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
	//switchLight(1);
	//delay(1);
	//switchLight(0);
}
