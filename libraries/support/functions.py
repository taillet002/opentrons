
def filteringParsXLS(dfSerie, idx, defaultVal):
    df = dfSerie.fillna(defaultVal)
    if type(df[idx]) == type('str'):
        if type(defaultVal) == type('str'):
            return df[idx]
        else:
            return defaultVal
    else:
        if type(defaultVal) == type('str'):
            return str(df[idx])
        else:
            return df[idx]


def logbook(comment='', _outputDest='computer'):
    """ function to print message with print() command in simulation mode or robot.comment() when robot is connected"""
    if _outputDest == 'robot':
        pass
        # robot.comment(comment)
    elif _outputDest == 'none':
        pass
    else:
        print(comment)


def ErrMessage(errId, messageAdd=''):
    """ function to display the error message according to the error Id and to execute block commands accordingly
        Args:
            errId: integer corresponding to the error idx
            messageAdd: string corresponding to specific message to send
    """
    if errId == 0:
        logbook('...ERROR : Server has returned a critical error : ' + messageAdd)
        raise NameError('Protocol is stopped due to :    ' + messageAdd)

    if errId == 1:
        msg = '...Error when loading labware ' + \
            messageAdd[0] + ' in position ' + messageAdd[1]
        logbook(msg)
        raise NameError('Protocol is stopped due to :    ' + msg)

    if errId == 2:
        msg = '...Error when loading Stock Solution ' + messageAdd
        logbook(msg)
        raise NameError('Protocol is stopped due to :    ' + msg)

    if errId == 3:
        msg = '...!!!!!! Warning: the Stock Solution ' + messageAdd + \
            ' in cmps_add_order variable is not present in the list of the Stock Solutions'
        logbook(msg)

    if errId == 4:
        msg = '...!!!!!! Error when mounting pipettes on the ' + messageAdd
        logbook(msg)
        raise NameError('Protocol is stopped due to :    ' + msg)

    if errId == 5:
        msg = '...Error when preparing CommanList with: ' + messageAdd
        logbook(msg)
        raise NameError('Protocol is stopped due to :    ' + msg)
    if errId == 6:
        msg = '...Error when generating commands for the robot '
        logbook(msg)
        raise NameError('Protocol is stopped due to :    ' + msg)
    if errId == 7:
        msg = '...Error when loading the custom rack : ' + messageAdd
        logbook(msg)
        raise NameError('Protocol is stopped due to :    ' + msg)
    if errId == 8:
        msg = '...!!!!!! Warning with the command : ' + messageAdd
        logbook(msg)
    if errId == 9:
        msg = '...!!!!!! Failed to execute ' + messageAdd
        logbook(msg)
    if errId == 10:
        msg = '...!!!!!! Error to generate command because stockSolution ' + \
            messageAdd + ' is no present in the stock solution definition'
        logbook(msg)
        raise NameError('Protocol is stopped due to :    ' + msg)
    if errId == 11:
        msg = '...!!!!!! Failed to execute liquidDeposing. Possible cause: error in rackName or droplets number higher than available location' + messageAdd
        logbook(msg)

def computeVolsFromPpm(dfStockSolutions, dfFormulations):
    df2 = dfStockSolutions
    df3 = dfFormulations
    df2['%A'] = df2['%A'].fillna(100)
    df2['density'] = df2['density'].fillna(1)
    # Loop on the row of df3 to convert ppm to vol in the row
    for idx0 in range(df3.shape[0]):
        # loop on each row of df2
        listStockSolutions = []
        listStockSolutionsExcept = []
        # Step 1 -> Male a List of the stockSolutioName and putting 'adjust' volume at the end
        for idx1 in range(df2['name'].size):
            # check if stockSolution exists in df3
            nameStockSolution = df2['name'][idx1]
            if nameStockSolution in df3:
                # check
                df3[nameStockSolution] = df3[nameStockSolution].fillna(0)
                if isinstance(df3[nameStockSolution][idx0], str):
                    listStockSolutionsExcept.append(idx1)
                else:
                    listStockSolutions.append(idx1)
        listStockSolutions += listStockSolutionsExcept
        # Step 2 -> We calculate the volumes
        massCumulated = 0
        massTarget = df3['mass'][idx0]
        for idSolution in listStockSolutions:
            nameStockSolution = df2['name'][idSolution]
            concentration = df2['%A'][idSolution]
            density = df2['density'][idSolution]
            # if value is 'adjust' str, we subtract the cumulate mass to the target mass and convert it into volume
            if isinstance(df3[nameStockSolution][idx0], str):
                if isinstance(massTarget, str):
                    print('Error, mass sample is not a number...')
                mass2Add = massTarget - massCumulated
                if mass2Add < 0:
                    mass2Add = 0
                    print('Warning....the mass to adjust is less than 0...')
                vol2Add = mass2Add/density
                dfCopy = df3[nameStockSolution].copy()
                dfCopy[idx0] = 1000*vol2Add
                df3[nameStockSolution] = dfCopy
                # df3.loc[idx0][nameStockSolution] = vo!l2Add*1000
            # if value is not 'adjust' str, then we convert to volume from equation and cumulate mass to subtract at the end
            else:
                ppm = df3[nameStockSolution][idx0]
                mass2Add = massTarget*(ppm/10000)/(concentration/100)/100
                massCumulated += mass2Add
                vol2Add = mass2Add/density
                dfCopy = df3[nameStockSolution].copy()
                dfCopy[idx0] = 1000*vol2Add
                df3[nameStockSolution] = dfCopy
    return df2, df3


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
        
    except (TypeError, ValueError):
        pass

    return False
