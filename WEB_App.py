# -*- coding: utf-8 -*-
import json
import math
import os
import time

import numpy as np
import pandas as pd
from aiohttp import web

from libraries.backlight.classBacklight import backlight
from libraries.camera.classCamera import webCam
from libraries.support.functions import ErrMessage, computeVolsFromPpm, filteringParsXLS, is_number
from libraries.uArm.classUarm import dictLetters2Num, dictNum2Letters, rack, uArmRobot

async def update(request):
    body = await request.json()
    if body['step'] == 'RequestingLabwares':
        try:
            fconfig = os.path.join(xlsPath, xlsFileConfig)
            df_labwares = pd.read_excel(
                open(fconfig, 'rb'), sheet_name='labwares')
            df_labwares['rackName'] = df_labwares['rackName'].fillna('None').str.lower()
            sub_df_labwares = df_labwares[df_labwares['rackModel'] == 'cupons-plate']
            N2add = sub_df_labwares.shape[0]
            N = df_labwares.shape[0]
            for idx in range(N2add):
                name = sub_df_labwares.iloc[idx]['rackName'] + '_ref'
                pos = sub_df_labwares.iloc[idx]['rackPos']
                df_labwares.loc[idx+N] = [name, 'ref_rack', pos]
            print('Sending Labwares configuration')
            data = {'done': True, 'commandExec': True,
                    'criticalError': False, 'dict_df': df_labwares.to_dict()}
        except Exception as e:
            print(e)
            print('Failed to request: Labwares')
            data = {'done': True, 'commandExec': False, 'criticalError': True,
                    'errDescription': 'file or tab cannot be found', 'dataframe': {}}

    if body['step'] == 'RequestingStockSolutions':
        try:
            frecipes = os.path.join(xlsPath, xlsFileRecipes)
            df_stockSolutions = pd.read_excel(
                open(frecipes, 'rb'), sheet_name='stock solutions definition')
            df_stockSolutions['%A'] = df_stockSolutions['%A'].fillna(100)
            df_stockSolutions['name'] = df_stockSolutions['name'].fillna('None').str.lower()

            df_stockSolutions['rackName'] = df_stockSolutions['rackName'].fillna('None').str.lower()

            df_stockSolutions['vialsPosition'] = df_stockSolutions['vialsPosition'].fillna('None').str.upper()

            df_stockSolutions['mixBeforeAdding'] = df_stockSolutions['mixBeforeAdding'].fillna('no').str.lower()

            print('Sending StockSolutions configuration')
            data = {'done': True, 'commandExec': True,
                    'criticalError': False, 'dict_df': df_stockSolutions.to_dict()}
        except Exception as e:
            print(e)
            print('Failed to request: StockSolutions')
            data = {'done': True, 'commandExec': False, 'criticalError': True,
                    'errDescription': 'file or tab cannot be found', 'dataframe': {}}

    if body['step'] == 'RequestingParameters':
        try:
            fconfig = os.path.join(xlsPath, xlsFileConfig)
            df_parameters = pd.read_excel(
                open(fconfig, 'rb'), sheet_name='parameters', index_col=0)

            # filtering
            listIndex = [df_parameters.index[i]
                         for i in range(df_parameters.shape[0])]

            dispense_method = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                'dispense_method'), defaultVal='transfer')  # transfer or distribute
            trash_policy = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                'trash_policy'), defaultVal='trash')  # transfer or distribute
            pipette_right_flowrates = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                'pipette_right_flowrates'), defaultVal='None')  # transfer or distribute
            pipette_left_flowrates = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                'pipette_left_flowrates'), defaultVal='None')  # transfer or distribute
            pipette_left_rack = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                'pipette_left_rack'), defaultVal='None').lower()  # transfer or distribute
            pipette_right_rack = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                'pipette_right_rack'), defaultVal='None').lower()  # transfer or distribute

            # we convert dataFrame to dict for sending to opentrons
            df_parameters = df_parameters.to_dict()
            df_parameters['parsValue']['dispense_method'] = dispense_method
            df_parameters['parsValue']['trash_policy'] = trash_policy
            df_parameters['parsValue']['pipette_right_flowrates'] = pipette_right_flowrates
            df_parameters['parsValue']['pipette_left_flowrates'] = pipette_left_flowrates
            df_parameters['parsValue']['pipette_right_rack'] = pipette_right_rack
            df_parameters['parsValue']['pipette_left_rack'] = pipette_left_rack

            print('Sending Parametres configuration')
            data = {'done': True, 'commandExec': True,
                    'criticalError': False, 'dict_df': df_parameters}

        except Exception as e:
            print(e)
            print('Failed to request: Requesting Parameters')
            data = {'done': True, 'commandExec': False, 'criticalError': True,
                    'errDescription': 'file or tab cannot be found', 'dataframe': {}}

    if body['step'] == 'RequestingCommandsList':

        fconfig = os.path.join(xlsPath, xlsFileConfig)
        frecipes = os.path.join(xlsPath, xlsFileRecipes)

        try:
            df_formulations = pd.read_excel(
                open(frecipes, 'rb'), sheet_name='formulations')
            df_parameters = pd.read_excel(
                open(fconfig, 'rb'), sheet_name='parameters', index_col=0)
            df_SOP = pd.read_excel(open(frecipes, 'rb'), sheet_name='SOP')
            df_stockSolutions = pd.read_excel(
                open(frecipes, 'rb'), sheet_name='stock solutions definition')

            # get Parameter to check if values have to be corrected
            listIndex = [df_parameters.index[i] for i in range(df_parameters.shape[0])]
            cmps_add_policy = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index('cmps_add_policy'), defaultVal='column')
            stockSolutions_mixing_pipette = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index('stockSolutions_mixing_pipette'), defaultVal='largest')

            transform_values = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index('transform_values'), defaultVal='None')
            droplets_density = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index('droplets_density'), defaultVal='2,2')
            droplets_dispensing_rate = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index('droplets_dispensing_rate'), defaultVal=0.2)
            droplets_airGap = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index('droplets_airGap'), defaultVal=0)
            
            #droplets_dispense_tip_height = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index('droplets_dispense_tip_height'), defaultVal=1)

            
            if transform_values == 'actizone':
                print('Checking File and convert ppm to vol')
                df_stockSolutions, df_formulations = computeVolsFromPpm(
                    df_stockSolutions, df_formulations)
                # Save Excel file to check for the user
                print('ppm were converted into volumes, it can be check in the following file:',
                      xlsFileSaveCorrectedRecipes)
                df_formulations.to_excel(
                    xlsFileSaveCorrectedRecipes, sheet_name='Volumes')

            # get usefull dictionnaries from Opentrons request
            pipetteItems = body['pars']['pipettesDict']
            valsplit, valsplitIdxMin, valsplitIdxMax = pipetteItems[
                'valsplit'], pipetteItems['valsplitIdxMin'], pipetteItems['valsplitIdxMax']
            volumesMax = pipetteItems['volumesMax']
            stockSolutionsItems = body['pars']['stockSolutionsItems']

            # dictionnary structure generation
            commandsList = {'commandName': [], 'stockSolutionName': [], 'rackSource': [], 'wellsSource': [], 'volDest': [], 'rackDest': [], 'wellsDest': [], 'pipette': [], 'done': [], 'rep': [], 'changeTip': [], 'rackSrcPosTip': [], 'rackDestPosTip': [], 'custom1': [], 'custom2': [], 'ratioFlowrates': []}

            def fillCommandsList(commandName=None, stockSolutionName=None, rackSource=None, wellsSource=None, volDest=None, rackDest=None, wellsDest=None,
                                 pipette=None, rep=None, done=None, changeTip=None, rackSrcPosTip=None, rackDestPosTip=None, custom1=None, ratioFlowrates=None,custom2=None):
                commandsList['commandName'].append(commandName)
                commandsList['stockSolutionName'].append(stockSolutionName)
                # Rack dest and source are the same here
                commandsList['rackSource'].append(rackSource)
                commandsList['wellsSource'].append(wellsSource)  # wells
                commandsList['volDest'].append(volDest)  # Volumes to dispense here
                # rack source and destination are the same
                commandsList['rackDest'].append(rackDest)
                commandsList['wellsDest'].append(wellsDest)  # wells
                commandsList['pipette'].append(pipette)
                commandsList['rep'].append(rep)
                commandsList['done'].append(done)
                commandsList['changeTip'].append(changeTip)
                commandsList['rackSrcPosTip'].append(rackSrcPosTip)
                commandsList['rackDestPosTip'].append(rackDestPosTip)
                commandsList['ratioFlowrates'].append(ratioFlowrates)
                commandsList['custom1'].append(custom1)
                commandsList['custom2'].append(custom2)
                # print(tipPolicy)

            def split_df(df, keyVal, val):
                df1 = df[df[keyVal] <= val]
                df2 = df[df[keyVal] > val]
                return df1, df2

            def waitingCommandGeneration(time):
                fillCommandsList(commandName='Waiting', stockSolutionName=time)

            def addingCommandGeneration(stockSolutionName, mixing, rackSrcPosTip, rackDestPosTip, ratioFlowrates=1):
                df_formulations[stockSolutionName] = df_formulations[stockSolutionName].fillna(0)
                for rackDest in listRackDest:
                    # We select a subset of main dataframe (we create as much as the number of different rackDest)
                    sub_df_formulations = df_formulations[df_formulations['rackDest'] == rackDest]
                    
                    # We select only rows where rackDest & vialsPos are not None
                    sub_df_formulations = sub_df_formulations[sub_df_formulations['vialsPos'] != 'nan']
                    sub_df_formulations = sub_df_formulations[sub_df_formulations['mass'] != 'nan']

                    # We select only volumes higher than 0
                    ssub_df_formulations = sub_df_formulations[sub_df_formulations[stockSolutionName] > 0]

                    # Build commands based on pipettes volumes and others
                    sssub_lower, sssub_upper = split_df(
                        ssub_df_formulations, stockSolutionName, valsplit)
                    rep = None
                    vol = None
                    if mixing:
                        rep = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index('stockSolutions_mixing_repeat'), defaultVal=5)
                        vol = None
                        # pipetteItems[valsplitIdxMin].max_volume
                        # print('ok')
                    if len(sssub_lower[stockSolutionName].values):
                        vialsList = [i if (type(i) == str) else int(i) for i in sssub_lower['vialsPos'].values]
                        fillCommandsList(commandName='Adding', rackSrcPosTip=rackSrcPosTip, rackDestPosTip=rackDestPosTip, stockSolutionName=stockSolutionName, rackSource=stockSolutionsItems[stockSolutionName]['rackName'], wellsSource=stockSolutionsItems[stockSolutionName]['vialsPosition'], volDest=sssub_lower[stockSolutionName].values.tolist(), rackDest=rackDest, wellsDest=vialsList, pipette=valsplitIdxMin, rep=rep, changeTip=vol, ratioFlowrates=ratioFlowrates,custom1=0)

                    # Check the Volume to determine the best pipette
                    if len(sssub_upper[stockSolutionName].values):
                        vialsList = [i if (type(i) == str) else int(
                            i) for i in sssub_upper['vialsPos'].values]
                        fillCommandsList(commandName='Adding', rackSrcPosTip=rackSrcPosTip, rackDestPosTip=rackDestPosTip, stockSolutionName=stockSolutionName, rackSource=stockSolutionsItems[stockSolutionName]['rackName'], wellsSource=stockSolutionsItems[stockSolutionName]['vialsPosition'], volDest=sssub_upper[stockSolutionName].values.tolist(), rackDest=rackDest, wellsDest=vialsList, pipette=valsplitIdxMax, rep=rep, changeTip=vol, ratioFlowrates=ratioFlowrates,custom1=0)

            def mixingCommandGeneration(rep, vol, policy_mixing, changeTip, pipetteIdx, rackDestPosTip='0.2', rackDest=None, ratioFlowrates=1):
                # get the last samples prepared

                if rackDest == 'recipes':
                    for rackDest in listRackDest:
                        sub_df_formulations = df_formulations[df_formulations['rackDest'] == rackDest]
                        sub_df_formulations = sub_df_formulations[sub_df_formulations['vialsPos'] != 'nan']
                        if len(sub_df_formulations['vialsPos']):
                            wells = [i if (type(i) == str) else int(
                                i) for i in sub_df_formulations['vialsPos'].values]
                            fillCommandsList(commandName='Mixing', stockSolutionName='Mixing' + str(idx0), rackSource=rackDest, wellsSource=wells, volDest=vol, rackDest=rackDest,
                                             wellsDest=wells, pipette=pipetteIdx, rep=int(rep), changeTip=changeTip, rackDestPosTip=rackDestPosTip, ratioFlowrates=ratioFlowrates)
                else:
                    if policy_mixing[0] == 'all':
                        #policy_mixing = []
                        pass
                        # 'all' propertie used with a specified rackName is not yet implementer. We skip this command for now
                    else:
                        wells = [int(float(i)) if is_number(i) else i for i in policy_mixing]
                        fillCommandsList(commandName='Mixing', stockSolutionName='Mixing' + str(idx0), rackSource=rackDest, wellsSource=wells, volDest=vol, rackDest=rackDest,
                                         wellsDest=wells, pipette=pipetteIdx, rep=int(rep), changeTip=changeTip, rackDestPosTip=rackDestPosTip, ratioFlowrates=ratioFlowrates)

            def dropletsDeposingCommandGeneration(rackSrc, wellSrc, rackDest, wellDest, dropVol,stateMix,pipetteIdx,rackSrcPosTip, rackDestPosTip,ratioFlowrates=1,custom1=0,custom2=[(0,0)]):

                fillCommandsList(commandName='Adding', stockSolutionName='DropletsDeposing' + str(idx0), rackSource=rackSrc, wellsSource=wellSrc, volDest=dropVol, rackDest=rackDest,
                                         wellsDest=[wellDest], pipette=pipetteIdx, rackSrcPosTip=rackSrcPosTip, rackDestPosTip=rackDestPosTip,ratioFlowrates=ratioFlowrates,custom1=custom1,custom2=custom2)

            def samplesPicturesCommandGeneration(rackName, wells, custom1='no'):
                fillCommandsList(commandName='SamplesPictures', stockSolutionName='Samples_picture_' + str(idx0), rackDest=rackName, wellsDest=wells, custom1=custom1)  # volDest is used for mixing yes or niot

            def dropletsPicturesCommandGeneration(rackName,plaques,filename,offsetsCoord):
                fillCommandsList(commandName='DropletsPictures', stockSolutionName='Droplets_picture_' + str(idx0), rackDest=rackName, wellsDest=plaques,rackSource=offsetsCoord, custom1=filename)
                # We put the offsetsCoord in the rackSource key (to avoid creating specific attributes in commandList dict)
            print('## Generating Robot commands list...')

            # Distribute CommandList
            if cmps_add_policy == 'column':
                df_formulations['rackDest'] = df_formulations['rackDest'].str.lower()
                col_name_list = []
                list_forbidden_name = ['rackDest','vialsPos','mass','pH']
                for col_name in df_formulations.columns:
                    if not (col_name in list_forbidden_name):
                        col_name = col_name.lower()
                    col_name_list.append(col_name)
                df_formulations.columns = col_name_list 
                try:
                    df_formulations['vialsPos'] = df_formulations['vialsPos'].str.upper()
                except:
                    df_formulations['vialsPos'] = df_formulations['vialsPos']
                # Generate list of rack used for sample
                listRackDest = df_formulations['rackDest'].unique()
                try:
                    for idx0 in range(df_SOP.shape[0]):
                        if df_SOP['step_type'][idx0] == 'samplesPictures':
                            # vials Locations defaut = 'all'
                            parameter_1 = filteringParsXLS(dfSerie=df_SOP['parameter_1'], idx=idx0, defaultVal='all')
                            parameter_2 = filteringParsXLS(dfSerie=df_SOP['parameter_2'], idx=idx0, defaultVal='yes')  # mixing
                            parameter_3 = filteringParsXLS(dfSerie=df_SOP['parameter_3'], idx=idx0, defaultVal=3)  # n repeat for mixing
                            parameter_4 = filteringParsXLS(dfSerie=df_SOP['parameter_4'], idx=idx0, defaultVal='0.2') #tip height in the vial if mixing
                            parameter_4 = str(parameter_4).split(",")
                            # constructing list of the vials to mix
                            if parameter_1 == 'all':
                                listWells = []
                                for rackDest in listRackDest:  # we scan all the dest rack available in the list
                                    sub_df_formulations = df_formulations[df_formulations['rackDest'] == rackDest]
                                    sub_df_formulations = sub_df_formulations[
                                        sub_df_formulations['vialsPos'] != 'nan']

                                    # let's get the wells
                                    if len(sub_df_formulations['vialsPos']):
                                        listWells.append([i if (type(i) == str) else int(
                                            i) for i in sub_df_formulations['vialsPos'].values])
                            else:
                                # need to list the wells here
                                listWells = parameter_1.split(",")

                            for c, wells in enumerate(listWells):
                                rackName = listRackDest[c]
                                for well in wells:
                                    if parameter_2 == 'yes':  # if mixing yes, put a mixing command
                                        mixingCommandGeneration(rep=parameter_3, vol=volumesMax[1], policy_mixing=[well], changeTip='yes', pipetteIdx=valsplitIdxMax, rackDestPosTip=parameter_4, rackDest=rackName)
                                    samplesPicturesCommandGeneration(rackName, well, custom1=parameter_2)
                        
                        elif df_SOP['step_type'][idx0] == 'dropletsPictures':
                            parameter_1 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_1'], idx=idx0, defaultVal='None').lower() # rackDest name (rack to take picture on it)
                            parameter_2 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_2'], idx=idx0, defaultVal='A1').upper()  # whick cupon in the rack ? (address is A1,A2 etc.)
                            parameter_3 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_3'], idx=idx0, defaultVal='generic')  # filename
                            dropletsPicturesCommandGeneration(parameter_1+'_ref',parameter_2,parameter_3,offsets_droplets_rack)
                            

                        elif df_SOP['step_type'][idx0] == 'dropletsDeposing':
                           # vials Locations defaut = 'all'
                            parameter_1 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_1'], idx=idx0, defaultVal='all') # which vials in the recipes do we want to address ?
                            parameter_2 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_2'], idx=idx0, defaultVal='None').lower()  #rackDest name (rack to depose droplets)
                            parameter_3 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_3'], idx=idx0, defaultVal='A1').upper()  # whick cupon in the rack ? (address is A1,A2 etc.)
                            parameter_4 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_4'], idx=idx0, defaultVal=5) #vol of droplets (in µl)
                            parameter_5 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_5'], idx=idx0, defaultVal=1) # tip height for aspirating
                            parameter_6 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_6'], idx=idx0, defaultVal=2)  # tip height for liquid deposing
                            parameter_7 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_7'], idx=idx0, defaultVal=1)  # repeat droplets
                            parameter_3 = str(parameter_3).split(",")
                            if parameter_1 == 'all':
                                listWells = []
                                for rackDest in listRackDest:  # we scan all the dest rack available in the list
                                    sub_df_formulations = df_formulations[df_formulations['rackDest'] == rackDest]
                                    sub_df_formulations = sub_df_formulations[
                                        sub_df_formulations['vialsPos'] != 'nan']
                                    # let's get the wells and put it in a list
                                    if len(sub_df_formulations['vialsPos']):
                                        listWells.append([i if (type(i) == str) else int(i) for i in sub_df_formulations['vialsPos'].values])
                            else:
                                # need to list the wells here
                                listWells = parameter_1.split(",")
                            # We create mapping wells on plaque
                            list_coords_droplets = []
                            droplets_density = droplets_density.split(',')
                            droplets_density = (int(droplets_density[0]),int(droplets_density[1]))
                            centerX = 25/droplets_density[0]/2
                            centerY = 25/droplets_density[1]/2

                            xDroplets = np.linspace(0,25,droplets_density[0],endpoint=False) +  centerX + 1
                            yDroplets = np.linspace(0,25,droplets_density[1],endpoint=False) +  centerY  + 2.25 #offset

                            Xgrid,Ygrid = np.meshgrid(xDroplets,yDroplets)
                            Xgrid = np.reshape(Xgrid,(-1))
                            Ygrid = np.reshape(Ygrid,(-1))
                            
                            # We flatten the coordinates

                            # We calculate the coordinates of each droplets on the plaques
                            # The limiting number of droplets if the positions available for the droplets...
                            for c,plaqueId in enumerate(parameter_3):
                                for c1 in range(Xgrid.shape[0]):
                                    list_coords_droplets.append((plaqueId,Xgrid[c1],Ygrid[c1]))
                            
                            c1 = 0 #index to map the well in the dest
                            for c, wells in enumerate(listWells): #list of samples list
                                rackNameSrc = listRackDest[c]
                                
                                for well in wells: # We go through the list
                                    if (parameter_2 != 'none' and c1 < len(list_coords_droplets)):
                                        wellDest = []
                                        coordDest = []
                                        for c11 in range(parameter_7):
                                            wellDest.append(list_coords_droplets[c1][0])
                                            coordDest.append((list_coords_droplets[c1][1],list_coords_droplets[c1][2]))
                                            c1 += 1
                                        wellDest = ','.join(wellDest)
                                        dropletsDeposingCommandGeneration(rackSrc=rackNameSrc, wellSrc=str(well),rackDest=parameter_2, wellDest=wellDest, dropVol=parameter_4, stateMix=parameter_5,pipetteIdx=valsplitIdxMin,
                                        rackSrcPosTip=parameter_5, rackDestPosTip=parameter_6,ratioFlowrates=droplets_dispensing_rate,custom1=droplets_airGap,custom2=coordDest)
                                        
                                    else:
                                        ErrMessage(11, messageAdd='iteration: ' + str(c1))
                        elif df_SOP['step_type'][idx0] == 'adding':
                            
                            # stock Solution name
                            parameter_1 = df_SOP['parameter_1'][idx0]
                            parameter_1 = parameter_1.lower()
                            # height Tip from bottom (aspirate)
                            parameter_2 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_2'], idx=idx0, defaultVal=0.2)
                            # height tip from top (dispense)
                            parameter_3 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_3'], idx=idx0, defaultVal=0.2)
                            parameter_4 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_4'], idx=idx0, defaultVal=1.0)  # flowrates ratio
                            
                            # parameter_2 = filteringParsXLS(dfSerie=df_SOP['parameter_2'],idx=idx0,defaultVal='trash') #tipPolicy return trash                            
                            if parameter_1 in df_formulations:
                                try: 
                                    if stockSolutionsItems[parameter_1]['mixBeforeAdding'] == 'yes':

                                        # to use the largest pipette to mix
                                        if stockSolutions_mixing_pipette == 'largest':

                                            parameter_1b = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                                                'stockSolutions_mixing_repeat'), defaultVal=3)
                                            parameter_2b = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                                                'stockSolutions_mixing_vol'), defaultVal=volumesMax[1])

                                            parameter_3b = stockSolutionsItems[parameter_1]['vialsPosition'].split(",")
                                            parameter_4b = 'no'

                                            parameter_5b = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                                                'stockSolutions_mixing_rate'), defaultVal=1.0)  # flowrate ratio
                                            parameter_6b = filteringParsXLS(dfSerie=df_parameters['parsValue'], idx=listIndex.index(
                                                'stockSolutions_mixing_tip_height'), defaultVal='0.2')
                                            parameter_6b = str(
                                                parameter_6b).split(",")

                                            rackName = stockSolutionsItems[parameter_1]['rackName']
                                            mixingCommandGeneration(rep=parameter_1b, vol=parameter_2b, policy_mixing=parameter_3b, changeTip=parameter_4b,
                                                                    pipetteIdx=valsplitIdxMax, rackDest=rackName, rackDestPosTip=parameter_6b, ratioFlowrates=parameter_5b)
                                            addingCommandGeneration(
                                                stockSolutionName=parameter_1, mixing=False, rackSrcPosTip=parameter_2, rackDestPosTip=parameter_3, ratioFlowrates=parameter_4)

                                        else:  # we use the current pipette to mix
                                            addingCommandGeneration(
                                                stockSolutionName=parameter_1, mixing=True, rackSrcPosTip=parameter_2, rackDestPosTip=parameter_3, ratioFlowrates=parameter_4)

                                    else:
                                        
                                        addingCommandGeneration(
                                            stockSolutionName=parameter_1, mixing=False, rackSrcPosTip=parameter_2, rackDestPosTip=parameter_3, ratioFlowrates=parameter_4)
                                except Exception as e:
                                    print(e)
                                    # raise
                                    # Return error because maybe stockSolution is not present in stockSolution definition sheet
                                    ErrMessage(10, messageAdd=parameter_1)

                        elif df_SOP['step_type'][idx0] == 'mixing':
                            parameter_1 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_1'], idx=idx0, defaultVal=3)
                            parameter_2 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_2'], idx=idx0, defaultVal=volumesMax[1])
                            # put defulat rack in recipes ?
                            parameter_3 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_3'], idx=idx0, defaultVal='recipes') #rackName (if recipes, rack in recipes spreadsheet is used)
                            parameter_3 = parameter_3.lower()
                            parameter_4 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_4'], idx=idx0, defaultVal='ALL')  # vials locations  ('all' not yet implemented or A1,A2...)
                            parameter_4 = parameter_4.upper()    
                            parameter_5 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_5'], idx=idx0, defaultVal='yes') # if yes, change the tip between each vials
                            parameter_5 = parameter_5.lower()
                            parameter_6 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_6'], idx=idx0, defaultVal='0.2') # height tip location
                            parameter_6 = str(parameter_6).split(",")
                            parameter_4 = str(parameter_4).split(",")
                            parameter_7 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_7'], idx=idx0, defaultVal=1.0)  # flowrate ratio

                            # filteringParsXLS(dfSerie=df_SOP['parameter_5'],idx=idx0,defaultVal=0.2)
                            mixingCommandGeneration(rep=parameter_1, vol=parameter_2, policy_mixing=parameter_4, changeTip=parameter_5,
                                                    pipetteIdx=valsplitIdxMax, rackDestPosTip=parameter_6, rackDest=parameter_3, ratioFlowrates=parameter_7)
                        elif df_SOP['step_type'][idx0] == 'waiting':
                            parameter_1 = filteringParsXLS(
                                dfSerie=df_SOP['parameter_1'], idx=idx0, defaultVal=5)
                            waitingCommandGeneration(time=parameter_1)
                except Exception as e:
                    print(e)
                    ErrMessage(6, messageAdd=df_SOP['step_type'][idx0])
                print('Sending commands list to the robot')
                data = {'done': True, 'commandExec': True,
                        'criticalError': False, 'dict_df': commandsList}
        except Exception as e:
            print(e)
            print('Failed to request: commands list')
            data = {'done': True, 'commandExec': False, 'criticalError': True,
                    'errDescription': 'failed to generate command list', 'dataframe': {}}

    if body['step'] == 'StopUArmRobot':
        try:
            print('Stopping the uArmRobot')
            uArm1.disconnect()
            isConnected = uArm1.isConnected
            data = {'done': True, 'commandExec': True,
                    'criticalError': False, 'isConnected': isConnected}
        except Exception as e:
            print(e)
            data = {'done': True, 'commandExec': False, 'criticalError': False,
                    'errDescription': 'Failed to disconnect the uArm Robot', 'dataframe': {}}

    if body['step'] == 'RequestingPicture':
        switchCase = body['pars']['switchCase']
        if (switchCase == 'droplets'):
            filename = body['pars']['filename'] 
            cuponLoc = body['pars']['cuponLoc']
            try:
               print('take picture of cupon in pos: ' + cuponLoc)
               time.sleep(0.5)
               cam1.capture(2, name=filename + cuponLoc)
               data = {'done': True, 'commandExec': True, 'criticalError': False}
               time.sleep(0.5)

            except Exception as e:
                print(e)
                print("Failed to take a picture")
                data = {'done': True, 'commandExec': False,
                            'criticalError': False}

        elif (switchCase == 'samples'):
            if not uArm1.isConnected:
                print('Connecting to the uArmRobot')
                if not uArm1.connect():
                    print('failed to connec to the uArmRobot')

                    data = {'done':  True, 'commandExec': False,
                            'criticalError': False}
                else:
                    uArm1.swift.reset()
            if not bl.isConnected:
                print('Connecting to the backlight')
                if not bl.connect():
                    print('Failed to connect to the backlight')
                else:
                    bl.lighting(0)
                time.sleep(0.2)

            if not cam.isConnected:
                print('Connecting to the cam')
                if not cam.connect():
                    print('Failed to connect to the cam')

            if uArm1.isConnected:
                # try:
                # Get the pars values
                vialsLocations = str(
                    body['pars']['vialsLocations'])  # only one vial
                # not used here as we only have one rack
                rackName = body['pars']['rackName']
                print('Take a picture of vial in position (Opentron): ' + vialsLocations)
                # we transpose coordinates in the uArm reference frame
                vialsLocations = dict_mappingCoordinates[vialsLocations]

                # Start the backlight
                data = {'done': True, 'commandExec': True, 'criticalError': False}
                try:
                    uArm1.moveZ(zTarget=rack1.ZPos['top'])
                    uArm1.pickVial(rack1, pos=vialsLocations)
                    uArm1.moveXY(rack2)
                    if bl.isConnected:
                        if bl.lighting(1):
                            print("Turn On the backlight")
                        else:
                            print("Failed to turn On the backlight")
                    uArm1.moveXY(rack3)

                    if cam.isConnected:
                        try:
                            time.sleep(1)
                            cam.capture(2, name='vial_' + vialsLocations)
                        except Exception as e:
                            print(e)
                            print("Failed to take a picture")

                    uArm1.moveXY(rack2)
                    if bl.lighting(0):
                        print("Turn Off the backlight")
                    else:
                        print("Failed to turn Off the backlight")
                    uArm1.dropVial(rack1, pos=vialsLocations)
                    uArm1.swift.reset()
                    uArm1.moveZ(zTarget=rack1.ZPos['top'])
                    uArm1.moveXY(rack2)
                except Exception as e:
                    print(e)
                    bl.lighting(0)
                    data = {'done': True, 'commandExec': False,
                            'criticalError': False}

    if body['step'] == 'RequestingCustomLabwaresXLS':
        fcustomLabwares = os.path.join(xlsPath, xlsFileCustomLabwares)
        try:
            print('Reading xls file : ', fcustomLabwares)
            df = pd.read_excel(open(fcustomLabwares, 'rb'),
                               sheet_name='custom_labwares')
            print('Sending dataframes to opentron')
            data = {'done': True, 'commandExec': True,
                    'criticalError': False, 'listdf': df.to_json()}

        except Exception as e:
            print(e)
            print('Error when loading the file')
            data = {'done': True, 'commandExec': False, 'criticalError': True,
                    'errDescription': 'file or tab cannot be found', 'dataframe': {}}
            raise

    if body['step'] == 'RequestingConfigXLS':
        fconfig = os.path.join(xlsPath, xlsFileConfig)
        frecipes = os.path.join(xlsPath, xlsFileRecipes)

        try:
            print('Reading xls file : ', fconfig)
            print('Reading xls file : ', frecipes)
            df1 = pd.read_excel(open(fconfig, 'rb'), sheet_name='labwares')
            df2 = pd.read_excel(open(frecipes, 'rb'),
                                sheet_name='stock solutions definition')
            df3 = pd.read_excel(open(frecipes, 'rb'),
                                sheet_name='formulations')
            df4 = pd.read_excel(open(fconfig, 'rb'),
                                sheet_name='parameters', index_col=0)
            df5 = pd.read_excel(open(frecipes, 'rb'), sheet_name='SOP')
            #df6 = pd.read_excel(open(fconfig, 'rb'),sheet_name='custom_labwares')

            # get Parameter to check if values have to be corrected
            listIndex = [df4.index[i] for i in range(df4.shape[0])]
            transform_values = filteringParsXLS(dfSerie=df4['parsValue'], idx=listIndex.index(
                'transform_values'), defaultVal='None')

            if transform_values == 'actizone':

                print('Checking File and convert ppm to vol')
                df2, df3 = computeVolsFromPpm(df2, df3)

                # Save Excel file to check for the user
                print('ppm were converted into volumes, it can be check in the following file:',
                      xlsFileSaveCorrectedRecipes)
                df3.to_excel(xlsFileSaveCorrectedRecipes, sheet_name='Volumes')

            listdf = [df1.to_json(), df2.to_json(), df3.to_json(),
                      df4.to_json(), df5.to_json()]
            print('Sending dataframes to opentron')
            data = {'done': True, 'commandExec': True,
                    'criticalError': False, 'listdf': listdf}
        except Exception as e:
            print(e)
            print('Error when loading the file')
            data = {'done': True, 'commandExec': False, 'criticalError': True,
                    'errDescription': 'file or tab cannot be found', 'dataframe': {}}
            raise

    return web.json_response(data)

#to remove ?
def readConfig(filename, path=""):
    # Unpack data
    None


if __name__ == "__main__":

    # --------------
    # json file reading
    # ---------------------
    jsonConfigFile = 'config/webApp_config.json'
    print("Loading webApp configuration json file: ", jsonConfigFile)
    df_config = pd.read_json(jsonConfigFile)

    xlsPath = ''
    xlsFileConfig = df_config.loc['xlsFileConfig'][0]
    xlsFileCustomLabwares = df_config.loc['xlsFileCustomLabwares'][0]
    xlsFileRecipes = df_config.loc['xlsFileRecipes'][0]
    xlsFileSaveCorrectedRecipes = df_config.loc['xlsFileSaveCorrectedRecipes'][0]
    configFileRackSamples = df_config.loc['configFileRackSamples'][0]
    configFileBacklight = df_config.loc['configFileBacklight'][0]
    configFileiPath_1 = df_config.loc['configFileiPath_1'][0]
    dict_mappingCoordinates = df_config.loc['mapping_Opentron2uArm'][0]
    backlightPort = df_config.loc['backlightPort'][0]
    camPort_samples = df_config.loc['camPort_samples'][0]
    camPort_droplets = df_config.loc['camPort_droplets'][0]
    picturesPath_samples = df_config.loc['picturesPath_samples'][0]
    picturesPath_droplets = df_config.loc['picturesPath_droplets'][0]
    offsets_droplets_rack = df_config.loc['offsets_droplets_rack'][0] #str
    
    # --------------
    # uArm Robot connection and racks creation
    # ---------------------
    uArmPath = ''
    #json.load(open( "config/mapping_Opentron2uArm.json" ))
    uArm1 = uArmRobot(effector='gripper')

    print("uArm initialization")
    rack1 = rack(nCols=10, nRows=4, diameter=14, name='samples')
    rack2 = rack(nCols=1, nRows=1, diameter=14, name='iPath_1')
    rack3 = rack(nCols=1, nRows=1, diameter=14, name='backlight')
    rack1.loadConfig(path=uArmPath, filename=configFileRackSamples)
    rack2.loadConfig(path=uArmPath, filename=configFileiPath_1)
    rack3.loadConfig(path=uArmPath, filename=configFileBacklight)

    # --------------
    # backlight connection
    # ---------------------
    print("Backlight initialization")
    bl = backlight(port=backlightPort)

    # --------------
    # cam connection
    # ---------------------
    print("Webcam for samples initialization")
    cam = webCam(camId=camPort_samples, pathPictures=picturesPath_samples)
    cam.connect()
    if (cam.isConnected):
        print("Webcam for samples is connected")
    print("Webcam for droplets initialization")

    cam1 = webCam(camId=camPort_droplets, pathPictures=picturesPath_droplets)
    cam1.connect()
    if (cam1.isConnected):
        print("Webcam for droplets is connected")

    print("Starting web App")
    app = web.Application()
    app.router.add_post('/', update)
    web.run_app(app, host='0.0.0.0', port=80)
