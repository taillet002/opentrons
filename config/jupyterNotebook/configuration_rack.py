from opentrons import instruments, labware, robot
import random
import numpy as np
from opentrons.data_storage import database

name = ['wheaton-serum-bottle-30-ml','large-plate-8ml','stirrer-plate','cupons-plate','ref_rack']
grid = [(4,2),(4,10),(2,1),(5,3),(1,1)]

spacing = [(29.1,34.5),(31.875,26.65),(63.75,85.5),(25.5,28.5),(0,0)]
diameter = [34,32,42,0,0]
depth =  [90,60,52,16,50]
volume = [30,4,60,2,0]

for idx,name_ in enumerate(name):
    if name_ in labware.list(): database.delete_container(name_)
    labware.create(
        name_,  # name of you labware
        grid=grid[idx],        # number of (columns, rows)
        spacing=spacing[idx],   # distances (mm) between each (column, row)
        diameter=diameter[idx],         # diameter (mm) of each well
        depth=depth[idx],           # depth (mm) of each well
        volume=volume[idx])         # volume (�L) of each well